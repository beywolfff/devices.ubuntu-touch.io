import { componentScriptLoader } from "@logic/client/clientNavigation.js";

export default function registerHomepage() {
  componentScriptLoader(".devices-homepage", () => {
    document.getElementById("toggleDeviceChoose").onclick = (e) => {
      let sidebar = document.querySelector("#sidebarCollapse");
      sidebar?.classList.add("show");
      sidebar?.scrollIntoView();
    };
  });
}
