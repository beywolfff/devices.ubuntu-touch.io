import { componentScriptLoader } from "@logic/client/clientNavigation.js";

export default function registerHeader() {
  componentScriptLoader("header.header", () => {
    document.getElementById("toggle-headerNav").onclick = (e) => {
      e.preventDefault();
      let appBackdrop = document.getElementById("app-backdrop");
      if (appBackdrop) {
        appBackdrop.classList.toggle("backdrop");
        document.getElementById("headerNav").classList.toggle("show");
      }
    };
  });

  componentScriptLoader("body", () => {
    document.getElementById("app-backdrop").onclick = (e) => {
      let headerNav = document.getElementById("headerNav");
      if (!e.target.closest(".header") && headerNav) {
        headerNav.classList.remove("show");
        document.getElementById("app-backdrop").classList.remove("backdrop");
      }
    };
  });
}
