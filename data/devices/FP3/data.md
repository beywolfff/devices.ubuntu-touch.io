---
name: "Fairphone 3 and 3+"
deviceType: "phone"
description: "The Fairphone 3/3+ in combination with Ubuntu Touch is currently the best option, if you value sustainability, fair trade materials, repairability and independency from Google and the Android operating systems. With its Snapdragon octa-core chip it delivers a solid performance all around. The 18:9 IPS display with a pixel-density of 427ppi has great viewing angles and shows no color shifts regardless of the viewing angle. Additionaly Ubuntu Touch for the Fairphone 3 is already prepared to run the new Waydroid container, which lets you run the Android applications, which you otherwise can't live without."
buyLink: "https://shop.fairphone.com/de/fairphone-3-plus"
price:
  avg: 439
  currency: "EUR"
  currencySymbol: "€"
subforum: "65/fairphone-3"

deviceInfo:
  - id: "cpu"
    value: "Octa-Core Kryo 250 1.8GHz"
  - id: "chipset"
    value: "Qualcomm MSM8953 Snapdragon 632"
  - id: "gpu"
    value: "Adreno 506 650MHz"
  - id: "rom"
    value: "64 GB"
  - id: "ram"
    value: "4 GB"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "3040 mAh"
  - id: "display"
    value: "143 mm (5.65 in) : 2160x1080 (427 PPI) - LCD IPS Touchscreen"
  - id: "rearCamera"
    value: "12 MP (FP3) | 48 MP/12 MP (FP3+), LED flash"
  - id: "frontCamera"
    value: "8 MP (FP3) | 16 MP (FP3+), No flash"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "158 mm (6.22 in) (h), 71.8 mm (2.83 in) (w), 9.89 mm (0.39 in) (d)"
  - id: "weight"
    value: "187,4 g"
  - id: "releaseDate"
    value: "03.09.2019"

contributors:
  - name: "Luksus"
    role: "Developer"
    photo: "https://live.staticflickr.com/65535/50801596512_9b846d1a60_k.jpg"
    forum: "https://forums.ubports.com/user/luksus"

communityHelp:
  - name: "Telegram - Fairphone + Ubuntu Touch"
    link: "https://telegram.me/joinchat/AI_ukwlaB6KCsteHcXD0jw"

sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "fairphone"
  deviceSource: "fairphone_fp3"
  kernelSource: "android_kernel_fairphone_sdm632"
  showDevicePipelines: true
---
