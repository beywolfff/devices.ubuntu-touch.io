---
portType: "Halium 9.0"

portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "?"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "?"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "-"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "sdCard"
        value: "x"
      - id: "rtcTime"
        value: "+"
      - id: "shutdown"
        value: "+-"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "?"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+-"
      - id: "nfc"
        value: "?"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "-"
      - id: "fingerprint"
        value: "+"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+-"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
      - id: "wiredExternalMonitor"
        value: "x"
---

## Important Notes

Apart from the features listed above, there is a thing to note:  
<br />

Single SIM in Slot 2 is stable. However calling from Slot 1 causes the call to fail and sometimes restart the UI. Mobile data can work from both slots. However, calls are only stable from Slot 2. Yet to figure out the cause.

Proximity sensor appears to works, according to SensorStatus. But screen still doesnt shut off on calls.
