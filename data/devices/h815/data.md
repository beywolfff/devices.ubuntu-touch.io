---
name: "LG G4"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8992 Snapdragon 808"
  - id: "gpu"
    value: "Adreno 418"
  - id: "rom"
    value: "32GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 5.1.1 to 7.0"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: "1440x2560 pixels, 5.5 in"
  - id: "rearCamera"
    value: "16MP"
  - id: "frontCamera"
    value: "8MP"
contributors:
  - name: Ari Kröyer
sources:
  portType: "external"
externalLinks:
  - name: "Kernel source"
    link: "https://github.com/abkro/android_kernel_lge_msm8992/"
  - name: "Device source"
    link: "https://github.com/abkro/android_device_lge_h815/"
  - name: "Device source - common"
    link: "https://github.com/abkro/android_device_lge_g4-common/"
---
