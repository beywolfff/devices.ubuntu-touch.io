---
variantOf: "gram"
name: "Xiaomi Redmi Note 9S/9 Pro (India)"

deviceInfo:
  - id: "rearCamera"
    value: "64MP(wide), 8MP(ultrawide), 5MP(macro), 2MP(depth)"
---
