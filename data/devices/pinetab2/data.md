---
name: "PineTab2"
deviceType: "tablet"
buyLink: "https://pine64.com/"
price:
  avg: 160
  currency: "USD"
subforum: "87/pine64"
description: "The PineTab2 is PINE64's successor to the original PineTab Linux tablet computer, featuring a faster processor and better availability. The tablet is available in two configurations, 4GB of RAM and 64GB of internal storage or 8GB of RAM and 128GB of internal storage. The tablet ships with a detachable keyboard that doubles as a protective cover."

deviceInfo:
  - id: "cpu"
    value: "4x 1.8 GHz Cortex-A55"
  - id: "chipset"
    value: "Rockchip RK3566"
  - id: "gpu"
    value: "Mali-G52 MP2 800 MHz"
  - id: "rom"
    value: "64 GB / 128 GB"
  - id: "ram"
    value: "4 GB  LPDDR4 / 8 GB  LPDDR4"
  - id: "battery"
    value: "6000 mAh"
  - id: "display"
    value: '10.1" 1280×800 IPS'
  - id: "rearCamera"
    value: "Single 5MP, 1/4″, LED Flash"
  - id: "frontCamera"
    value: "Single 2MP, f/2.8, 1/5″"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "242mm x 161mm x 9mm"
  - id: "weight"
    value: "538 grams"
  - id: "releaseDate"
    value: "02.06.2023"

sources:
  portType: "external"
  issuesLink: "https://gitlab.com/ook37/pinephone-pro-debos/-/issues"

externalLinks:
  - name: "Sources"
    link: "https://gitlab.com/ook37/pinephone-pro-debos/"
  - name: "CI Builds"
    link: "https://gitlab.com/ook37/pinephone-pro-debos/-/releases"
    noActivityDetection: true

communityHelp:
  - name: "Pine64 subforum on UBports Forum"
    link: "https://forums.ubports.com/category/87/pine64"
  - name: "Pinetab subforum on Pine64 Forum"
    link: "https://forum.pine64.org/forumdisplay.php?fid=142"
  - name: "PineTab subreddit"
    link: "https://www.reddit.com/r/PineTab/"
  - name: "IRC ( #pinetab on irc.pine64.org )"
    link: "https://www.pine64.org/web-irc/"
  - name: "#pinetab on Pine64 Discord"
    link: "https://discord.gg/knRjbFe2jP"
  - name: "Telegram chat"
    link: "https://t.me/utonpine"

contributors:
  - name: "Pine64"
    role: "Phone maker"
  - name: "Oren Klopfer"
    role: "Developer"
---
