---
name: "Xiaomi Redmi Note 9"
deviceType: "phone"
image: "https://fdn2.gsmarena.com/vv/pics/xiaomi/xiaomi-redmi-note-9-4.jpg"

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Mediatek Helio G85"
  - id: "gpu"
    value: "Mali-G52 MC2"
  - id: "rom"
    value: "64GB/128GB"
  - id: "ram"
    value: "3GB/4GB/6GB"
  - id: "android"
    value: "Android 10 Miui 12"
  - id: "battery"
    value: "5020 mAh"
  - id: "display"
    value: "IPS LCD, 450 nits (typ) 6.53 inches, 104.7 cm2 (~83.5% screen-to-body ratio) 1080 x 2340 pixels, 19.5:9 ratio (~395 ppi density)"
  - id: "arch"
    value: "arm64"
  - id: "rearCamera"
    value: "48MP(wide), 8MP(ultrawide), 2MP(macro), 2MP(depth)"
  - id: "frontCamera"
    value: "13MP"
  - id: "dimensions"
    value: "162.3 x 77.2 x 8.9 mm (6.39 x 3.04 x 0.35 in)"
  - id: "weight"
    value: "199 g (7.02 oz)"
contributors:
  - name: TheKit
    forum: "https://forums.ubports.com/user/thekit"
  - name: Tim
    forum: "https://t.me/eintim23"
sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "xiaomi-redmi-9"
  deviceSource: "xiaomi-lancelot"
  kernelSource: "kernel-xiaomi-mt6768"
communityHelp:
  - name: "Device Support"
    link: "https://t.me/utlance"
---
