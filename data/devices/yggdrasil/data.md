---
name: "Volla Phone"
deviceType: "phone"
image: "https://volla.online/de/resources/Store/volla-phone.png"
buyLink: "https://volla.online/de/shop/volla-phone/"
description: "Volla Phone, A startup focused on simplicity and privacy for the average user. It’s a solid mid-range device with eight-core, produced by Gigaset. This device falls nicely in the hands, and it has an outstanding screen/body ratio. On the other hand, it has slight incompatibility with the UT navigation bar. But, overall, it is working very smoothly and relatively responsive."
tag: "promoted"
subforum: "90/vollaphone"
price:
  avg: 219
  currency: "EUR"
  currencySymbol: "€"

deviceInfo:
  - id: "cpu"
    value: "Octa-core ARM Cortex-A53 (4x 2.0 GHz + 4x 1.5 GHz cores)"
  - id: "chipset"
    value: "MediaTek Helio P23, MT6763V"
  - id: "gpu"
    value: "ARM Mali-G71 MP2 @ 770 MHz, 2 cores"
  - id: "rom"
    value: "64 GB, eMMC"
  - id: "ram"
    value: "4 GB, DDR3"
  - id: "android"
    value: "9.0 (Pie)"
  - id: "battery"
    value: "4700 mAh, 18.1 Wh, Li-Polymer"
  - id: "display"
    value: '6.3" IPS, 1080 x 2340 (409 PPI), V-notch, Rounded corners'
  - id: "rearCamera"
    value: "16MP (f/2.0, 1080p30 video) + 2MP (for bokeh/depth), PDAF, LED flash"
  - id: "frontCamera"
    value: "16MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "157 mm x 75.1 mm x 9.65 mm"
  - id: "weight"
    value: "190 g"
  - id: "releaseDate"
    value: "July 2020"

contributors:
  - name: "Hallo Welt Systeme UG"
    role: "Phone maker"
    forum: "https://volla.online/en/"
    photo: "https://avatars.githubusercontent.com/u/62447194" # https://github.com/HelloVolla
  - name: "TheKit"
    role: "Developer"
    forum: "https://forums.ubports.com/user/thekit"
  - name: "Deathmist"
    role: "Developer"
    forum: "https://forums.ubports.com/user/deathmist"
    photo: "https://forums.ubports.com/assets/uploads/profile/3171-profileavatar-1613145487767.png"

sources:
  portType: "reference"
  portPath: "android9"
  deviceGroup: "volla-phone"
  deviceSource: "android_device_volla_yggdrasil"
  kernelSource: "android_kernel_volla_mt6763"
  issuesRepo: "volla-yggdrasil"

externalLinks:
  - name: "Halium build manifest"
    link: "https://github.com/Halium/halium-devices/blob/halium-9.0/manifests/volla_yggdrasil.xml"
  - name: "UBports Installer config"
    link: "https://github.com/ubports/installer-configs/blob/master/v2/devices/yggdrasil.yml"
  - name: "Source for details on this page"
    link: "https://gitlab.com/ubports/infrastructure/devices.ubuntu-touch.io/-/tree/main/data/devices/yggdrasil"

communityHelp:
  - name: "Telegram - @utonvolla"
    link: "https://t.me/utonvolla"

seo:
  description: "Get your Volla Phone with latest version of Ubuntu Touch operating system, a private OS developed by hundreds of people."
  keywords: "Ubuntu Touch, VollaPhone, Volla Phone, Linux Phone"
---
