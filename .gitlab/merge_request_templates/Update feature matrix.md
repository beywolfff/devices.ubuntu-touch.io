# Feature matrix update

Device codename: "codename of your device"

Device repository: "github.com/"

Release channels available: "stable|release candidate|development|edge"

Default release channel: "stable"

When will this update be released to the default release channel? "OTA17"

## Notes:
